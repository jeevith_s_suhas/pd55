#include<stdio.h>
int input()
{
  int n;
  printf("Enter the year to check if it is a leap year\n");
  scanf("%d",&n);
  return n;
}
int check(int n)
{
  int c=0;
  if(n%4==0)
  {
   c++;
   if(n%100==0)
   { c--;}
   else
   {c++;}
  }
  return c;
}
void display(int n,int c)
{
  if(c>0)
  {printf("The year %d is a leap year\n",n);}
  else
  {printf("The year %d is not  a leap year\n",n);}
}
int main()
{
  int n=input();
  int c=check(n);
  display(n,c);
  return 0;
}