#include <math.h> 
#include <stdio.h>
struct point
{
  float x;
  float y;
};
typedef struct point Point;
Point input()
{
   Point a;
   printf("Enter the co-ordinates of the point\n");
   scanf("%f %f",&a.x,&a.y);
   return a;
}
float distance(Point p1,Point p2)
{
   float d;
   d=sqrt((p2.x-p1.x)*(p2.x-p1.x)+(p2.y-p1.y)*(p2.y-p1.y));
   return d;  
}
void output(Point p1,Point p2,float d)
{
   printf(" The distance between the points (%f,%f)and (%f,%f) is %f",p1.x,p1.y,p2.x,p2.y,d);
  
}
int main()
{
    Point p1,p2;
    float d;
    p1=input();
    p2=input();
    d=distance(p1,p2);
    output(p1,p2,d);
    return 0;
}

