#include<stdio.h>
int input()
{
  int a;
  printf("Enter the number\n");
  scanf("%d",&a);
  return a;
}
void swap(int *a,int *b)
{
  int temp=0;
  temp=*a;
  *a=*b;
  *b=temp;
}
void display(int x,int y)
{
  printf("The swapped numbers are %d and %d\n",x,y);
  
}
int main()
{
  int c,d;
  c=input();
  d=input();
  swap(&c,&d);
  display(c,d);
  return 0;

}