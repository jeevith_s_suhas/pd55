#include<stdio.h>
int input()
{
  int n;
  printf("Enter an integer to find the sum of digits\n");
  scanf("%d",&n);
  return n;
}
int compute(int n)
{
  int s=0,d=0;
  while(n!=0)
  {
    d=n%10;
    s+=d;
    n/=10; 
  
  }
  return s;
}
void display(int n,int s)
{
  printf("The sum of the digits of %d is %d\n",n,s);
}
int main()
{
  int n,s;
  n=input();
  s=compute(n);
  display(n,s);
  return 0;
}