#include<stdio.h>
int input()
{
  int n;
  printf("Enter a number");
  scanf("%d",&n);
   return n;
}
int reversej(int n)
{
   int r=1;
   int s=0;
   while(n!=0)
   {
     r=n%10;
     s=r+(s*10);
     n/=10;
   }
  return s;
}
void display(int n,int s)
{
  printf("The reversed of number %d is %d",n,s);
}
int main()
{
  int n=input();
  int s=reversej(n);
  display(n,s);
   return 0;

}