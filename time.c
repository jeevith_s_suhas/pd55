#include<stdio.h>
struct time
{
   int h,m;
};
struct time input()
{
  struct time t;
  printf("Enter the time in hours and minutes\n");
  scanf("%d%d",&t.h,&t.m);
  return t;
}
int compute(struct time t)
{
  int s;
  s=t.h*60+t.m;
  return s;
}
void print(struct time t,int minutes)
{
  printf("The time %d hours and %d minutes is equivalent to %d minutes\n",t.h,t.m,minutes);

}
int main()
{
  struct time t;
  int minutes ;
  t = input();
  minutes=compute(t);
  print(t,minutes);
  return 0;

}